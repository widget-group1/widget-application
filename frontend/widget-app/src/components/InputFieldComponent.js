import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import FormHelperText from '@mui/material/FormHelperText';
import TextField from '@mui/material/TextField';

export const InputFieldComponent = props => {

    const {id="", title="", label="", variant="", required=false, onChange=()=>{}, className, placeholder="", helperText="", wrapperclass="", value=""} =props;
    return (
        <div className={wrapperclass}>
            <InputLabel htmlFor="component-helper">{title}</InputLabel>
            <TextField
                id={id}
                label={label}
                variant={variant}
                required={required}
                onChange={onChange}
                className={className}
                placeholder={placeholder}
                value={value}
            />
            {helperText && <FormHelperText id="component-helper-text">
                {helperText}
            </FormHelperText>}
        </div>
    );
}
