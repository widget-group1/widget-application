import * as React from 'react';
import InputLabel from '@mui/material/InputLabel';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import FormControl from "@mui/material/FormControl";
import FormHelperText from '@mui/material/FormHelperText';


export const DropdownComponent = props => {
    const { label, wrapperclass="", options, helperText="", update=()=>{}, required=false, FormControlClass=""} = props;
    const [type, setType] = React.useState("");

    return( 
            <div className={wrapperclass}>
                <FormControl className={FormControlClass}>
                    <InputLabel id="select-helper-label">{`${label}*`}</InputLabel>
                    <Select
                        labelId="select-helper-label"
                        id="select-helper"
                        value={type}
                        required={required}
                        label={label}
                        onChange={ event => {
                            const val = event.target.value;
                            setType(val);
                            update(val);
                        }}
                        className={"select-style"}
                    >      
                        {options.map((item, i) =><MenuItem key={i+1} value={item.value}>{item.label}</MenuItem>)}

                    </Select>
                    {helperText && <FormHelperText>
                        {helperText}
                    </FormHelperText>}
                </FormControl>
            </div>
             
        );
}