import * as React from 'react';
import FormHelperText from '@mui/material/FormHelperText';
import TextField from '@mui/material/TextField';

export const TextAreaComponent = props => {

    const {helperText, wrapperclass, rows=4, value="", label="", id="", placeholder="", className, onChange=()=>{}}=props;
    return (
        <div className={wrapperclass}>
            <TextField
                required
                multiline
                id={id}
                label={label}
                rows={rows}
                placeholder={placeholder}
                value={value}
                onChange={onChange}
                className={className}
            />
            {helperText && <FormHelperText id="component-helper-text">
                {helperText}
            </FormHelperText>}
        </div>
    );
}
