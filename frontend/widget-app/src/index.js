import React from 'react';
import ReactDOM from 'react-dom/client';
import Routers from './Routers';
import { Provider } from 'react-redux';
import { store, persistor } from './store';
import { PersistGate } from 'redux-persist/integration/react';
import reportWebVitals from './reportWebVitals';
import {AuthProvider} from "react-auth-kit";
import { LabelProvider } from "./containers/ArticleContainer/Stepper/labelDataContext";

import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <LabelProvider>
      <Provider store={store}>
          <PersistGate persistor={persistor}>
              <AuthProvider authType="cookie" authName="_auth" cookieDomain={window.location.hostname} cookieSecure={false} >
                  <Routers/>
              </AuthProvider>
          </PersistGate>
      </Provider>
      </LabelProvider>
  </React.StrictMode>
);

reportWebVitals();
