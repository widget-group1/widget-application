import React from "react";
import { Router, Route, Routes } from 'react-router-dom';
import history from './history';
import App from './App';
import { Login } from "./Login";
import SectionMdHtml from "./containers/SectionMdHtml";
import HomeArticles from "./HomeArticles";
import ArticleTabComponent from "./containers/ArticleContainer/Stepper/ArticleTabComponent";

const Routers = () => {
    return (
        <Router location={history.location} navigator={history} history={history}>
            <Routes>
                <Route exact path="/" element={<Login/>} />
                <Route exact path="/logout" element={<div> <h2>You have successfully logged out ! Please visit again. </h2></div>} />
                <Route exact path="/home" element={<App/>} />
                <Route exact path="/publish" element={<App/>} />
                <Route exact path="/createArticle" element={<SectionMdHtml/>} />
                <Route exact path="/articles" element={<HomeArticles/>} />
                <Route exact path="/report" element={<div/>}/>
                <Route exact path="/create-article" element={<ArticleTabComponent/>}/>
            </Routes>
        </Router >)
}



export default Routers;