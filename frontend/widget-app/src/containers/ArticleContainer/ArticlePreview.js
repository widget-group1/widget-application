import React, {useContext} from "react";
import { LabelContext } from "./Stepper/labelDataContext";
import ReactMarkdown from "react-markdown";

const ArticlePreview =()=>{
    const value = useContext(LabelContext);
    const {labelInfo}=value;
    const {articleData, articleInfo}=labelInfo;

    return (
        <div className="article-preview">
            <div className="preview-info">
                <span >{`Title:\t ${articleInfo.title}`}</span>
                <div >{`Summary:`}</div>
                <div className="preview-info-val">{`${articleInfo.summary}`}</div>
                
            </div>
            <div className="preview-data">
                <ReactMarkdown>{articleData}</ReactMarkdown>
            </div>

        </div>
    )
}

export default ArticlePreview;