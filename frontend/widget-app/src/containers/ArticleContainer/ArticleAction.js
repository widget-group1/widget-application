import * as React from 'react';

import Grid from '@mui/material/Grid'; // Grid version 1
import { ButtonComponent } from '../../components/ButtonComponent';


export const ArticleAction = props => {

    const {value, isSubmit, wrapperclass=""} = props;

    return( <Grid container spacing={2} className={wrapperclass}>
            
            <Grid item xs={10}/>
            <Grid item xs={2} className="action-buttons-wapper">
                <ButtonComponent
                    disabled={false}
                    onClick={() => value.saveDraft()}
                    >
                    Save Draft
                </ButtonComponent>
                <ButtonComponent
                    disabled={false}
                    onClick={() => value.cancel()}
                    >
                    Cancel
                </ButtonComponent>
                {isSubmit && <ButtonComponent
                    disabled={false}
                    onClick={e => value.submit()}
                    >
                    Submit
                </ButtonComponent>}
            </Grid>
        </Grid> 
    );
}