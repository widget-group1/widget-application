
import React, {useContext} from "react";
import Stepper from "react-stepper-horizontal";
import { LabelContext } from "./labelDataContext";

const StatusStepperComponent = props => {
  
    const contextValue = useContext(LabelContext);
    const {steps} = contextValue;
    const {currentStep} = props;

    const currStep = steps.filter(item => item.value === currentStep);
    const stepnumber = currStep ? currStep[0].step : 0;


    
    return (
        <div className="stepper-nav">
            <Stepper 
                steps={steps}
                activeStep={stepnumber}
                activeColor={"#3ec153"}
                completeColor={"#3ec153"}
            />
        </div>
      
    )
    
  }

  export default StatusStepperComponent;