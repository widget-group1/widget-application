import React, { useState, createContext } from "react";

export const LabelContext = createContext();

export const LabelProvider = (props) => {

  const [labelInfo, setlabelInfo] = useState({
    articleData: "",

    articleInfo: {
        title: "",
        type: "",
        summary: "",
        cbDQuery: "",
        tHQuery: "",
        link: ""
    },
    images:[],
  });

  const steps= [
    { 
        title: "Draft",
        value: "Draft",
        step: 0,
        icon: "./images/ok--v1.png"

    },
    { 
        title: "In Review" ,
        value: "InReview",
        step: 1,
        icon: "./images/ok--v1.png"
    },
    { 
        title: "Approved" ,
        value: "Approved" ,
        step: 2,
        icon: "./images/ok--v1.png"
    },
    { 
        title: "Ready To Publish" ,
        value: "ReadyToPublish",
        step: 3,
        icon: "./images/ok--v1.png"
    },
    { 
        title: "Published" ,
        value: "Published",
        step: 4,
        icon: "./images/ok--v1.png"
    }
  ];

  const cancel = () => {
  };
  const saveDraft = () => {
  };

  const handleChange = prop => event => {
    setlabelInfo({ ...labelInfo, [prop]: event.target.value });
  };

  const setInfo = prop => event => {
    setlabelInfo({
      ...labelInfo,
      articleInfo: { ...labelInfo.articleInfo, [prop]: event.target.value }
    });
  };

  const setType = (prop, data) =>{
    setlabelInfo({
      ...labelInfo,
      articleInfo: { ...labelInfo.articleInfo, [prop]: data }
    });
  }

  const setData = data => {
    setlabelInfo({
      ...labelInfo,
      articleData: data
    });
  };

  const setImages = imgList => {
    setlabelInfo({
      ...labelInfo,
      images: imgList
    });
  };

  return (
    <LabelContext.Provider
      value={{
        cancel,
        steps,
        saveDraft,
        labelInfo,
        handleChange,
        setInfo,
        setData,
        setType,
        setImages
      }}
    >
      {props.children}
    </LabelContext.Provider>
  );
};
