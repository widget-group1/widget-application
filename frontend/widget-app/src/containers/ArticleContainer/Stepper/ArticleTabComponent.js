
import React, {useContext, useState} from "react";
import PropTypes from 'prop-types';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Grid from '@mui/material/Grid'; // Grid version 1

import InfoIcon from '@mui/icons-material/Info';
import AssignmentIcon from '@mui/icons-material/Assignment';
import PreviewIcon from '@mui/icons-material/Preview';

import ArticleInfo from "../ArticleInfo";
import ArticleData from "../ArticleData";
import { LabelContext } from "./labelDataContext";
import ScreenTop from "../../ScreenTop";
import StatusStepperComponent from "./StatusStepperComponent";
import { ArticleAction } from "../ArticleAction";
import ArticlePreview from "../ArticlePreview";


const ArticleTabComponent = () => {

  
  const userName = sessionStorage.getItem("name");
  const contextValue = useContext(LabelContext);
  console.log("LabelContext::::", LabelContext);

  const [stepValue, setStepValue] = useState(0);

  const handleChange = (event, stepValue) => {
    setStepValue(stepValue);
  };

  const TabContainer = (props)=> {
    return (
      <>
        {props.children}
      </>
    );
  }
  
  TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
  };

    return (
      <>
        <ScreenTop userName={userName}/>
        <StatusStepperComponent currentStep={"InReview"}/>
        <Grid className="grid-container">
                <Tabs
                  value={stepValue}
                  onChange={handleChange}
                  variant="scrollable"
                  indicatorColor="primary"
                  textColor="primary"
                  className="article-tabs-section"   
                >
                    <Tab label="Article Info" iconPosition="start" icon={<InfoIcon />} />
                    <Tab label="Article Data" iconPosition="start" icon={<AssignmentIcon />} />
                    <Tab label="Preview" iconPosition="start" icon={<PreviewIcon />} />
                
                </Tabs>
                
                {stepValue === 0 && 
                    <>
                        <ArticleInfo/>
                        <ArticleAction 
                            wrapperclass="padding-actions-acticle-info" 
                            value={contextValue} 
                        />
                    </>
                
                }
                {stepValue === 1 && 
                    <>
                        <ArticleData/>
                        <ArticleAction 
                            wrapperclass="padding-actions-acticle" 
                            value={contextValue} 
                        />
                    </>
                }
                {stepValue === 2 && 
                    <>
                        <ArticlePreview/>
                        <ArticleAction 
                            wrapperclass="padding-actions-acticle"
                            value={contextValue} 
                            isSubmit={true}
                        />
                    </>
                }
            
        </Grid>
      </>
    )
    
  }

  export default ArticleTabComponent;