import React, {useContext, useState} from "react";
import { LabelContext } from "./Stepper/labelDataContext";
import { InputFieldComponent } from "../../components/InputFieldComponent";
import { DropdownComponent } from "../../components/DropdownComponent";
import { TextAreaComponent } from "../../components/TextAreaComponent";
import FileUpload from "react-material-file-upload";

const ArticleInfo = () =>{
    const value = useContext(LabelContext);
    const [files, setFiles] = useState();
    const articleInfo = value.labelInfo.articleInfo;
    const categoryOptions = [
        { label: "TAU-TIN", value: "tautin" },
        { label: "BULLETIN", value: "bulletin" }
      ];

    return(
            <div className="article-info">
                <div className="info-section">
                    <DropdownComponent
                        label="Type"
                        required
                        options={categoryOptions} 
                        update={data => value.setType("type", data)}
                        value = {articleInfo.type}
                        placeholder="enter Type..."
                        FormControlClass="style-type"
                        wrapperclass="wrapperclass"
                    />
                    <InputFieldComponent
                        id="standard-title"
                        variant="outlined" 
                        label="Title"
                        required   
                        value={articleInfo.title}
                        onChange={value.setInfo("title")}
                        className="article-info-item"
                        placeholder="enter Title..."
                        wrapperclass="wrapperclass"
                    />  

                    <TextAreaComponent
                        required
                        id="textarea-summary"
                        rows={4}
                        title="Summary"
                        label="Enter Summary "
                        onChange={value.setInfo("summary")}
                        value={articleInfo.summary}
                        className="article-info-item"
                        placeholder="enter Summary..."
                        wrapperclass="wrapperclass"
                    />
                    <div className="info-fields">
                        <TextAreaComponent
                            required
                            id="CbDQuery"
                            rows={4}
                            title="CbD Query"
                            label="Enter CbD Query "
                            onChange={value.setInfo("cbDQuery")}
                            value={articleInfo.cbDQuery}
                            className="article-info-item"
                            placeholder="enter CbDQuery..."
                            wrapperclass="wrapper-split"
                        />

                        <TextAreaComponent
                            required
                            id="THQuery"
                            rows={4}
                            title="TH Query"
                            label="Enter TH Query "
                            onChange={value.setInfo("tHQuery")}
                            value={articleInfo.tHQuery}
                            className="article-info-item"
                            placeholder="enter THQuery..."
                            wrapperclass="wrapper-split"
                        />
                    </div>
                    <div className="upload-section">
                        <FileUpload value={files} onChange={setFiles} >Drag and drop the Files Here</FileUpload>
                    </div>
                    
                </div>
            </div>
            
    )
}

export default ArticleInfo;