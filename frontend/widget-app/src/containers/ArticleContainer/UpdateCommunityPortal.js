import React from "react";
import { ButtonComponent } from "../../components/ButtonComponent";
import { InputFieldComponent } from "../../components/InputFieldComponent";

const UpdateCommunityPortal = props =>{
    const {userName} = props;
    return(
        <div className="screen-top">
            <div>
                <h3>Approved Article</h3>
                <div>Publish the article to community portal and provide the link below</div>
                <div>
                    <span>Community Portal Link</span>
                    <InputFieldComponent/>
                </div>
                <div>
                    <ButtonComponent onClick={()=>{}}>
                        Submit
                    </ButtonComponent>
                    <ButtonComponent onClick={()=>{}}>
                        Cancel
                    </ButtonComponent>
                </div>
            </div>
        </div>
    );
}

export default UpdateCommunityPortal;