import React, {useState, useContext}  from "react";
import ReactMarkdown from "react-markdown";
import PropTypes from 'prop-types';
import Tooltip from '@mui/material/Tooltip';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Dropzone from 'react-dropzone';
import IconButton from '@mui/material/IconButton';
import PhotoCameraIcon from '@mui/icons-material/PhotoCamera';

import '../style.css';
import { LabelContext } from "./ArticleContainer/Stepper/labelDataContext";
import { ButtonComponent } from "../components/ButtonComponent";

const SectionMdHtml = () =>{

    const value = useContext(LabelContext);
    const {labelInfo, setData, setImages} = value;
    const {articleData="", images=[]} = labelInfo;
    const [markDown, setMarkDown] = useState(articleData);
    const [stepValue, setStepValue] = useState(0);
    const [files, setFiles] = useState(images);

    const handleChange = (event, stepValue) => {
        setStepValue(stepValue);
    };

    const onDrop = (acceptedFiles) => {
        const allFiles = [...files, ...acceptedFiles];
        setFiles(allFiles);
        setImages(allFiles);
    }

    const uploadSelected = ()=>{
        console.log("generate links for the files");
        //call api to generate links with all files
    }
    
    return (
        <div className="article-data-section-MdHtml">
            <Tabs
                value={stepValue}
                onChange={handleChange}
                variant="scrollable"
                indicatorColor="primary"
                textColor="primary"
                orientation="vertical"
            >
                <Tab label="Markdown" />
                <Tab label="HTML Preview" />
                <Tab label="Images" />
            
            </Tabs>
            <div>
            {stepValue === 0 && 
                <div className="markdown-section">
                    <textarea 
                        className="markdown-section-textarea"
                        value={markDown}
                        onChange={e=> {
                            setMarkDown(e.target.value);
                            setData(e.target.value);
                        }}
                    >
                    </textarea>
                </div>
            }
            {stepValue === 1 && 
                <div className="html-section">
                    <ReactMarkdown>{markDown}</ReactMarkdown>
                </div>
            }
            {stepValue === 2 && 
                <div className="html-section">
                    <div className="dropzone">
                        <Dropzone  
                            accept="image/png, image/jpg, image/jpeg"
                            onDrop={onDrop}
                            multiple
                            >
                            {({getRootProps, getInputProps}) => (
                                <div {...getRootProps()}>
                                <input {...getInputProps()} />
                                    <span className="icon-dropzone-style">
                                        <IconButton sx={{ color: "#00567a" }} aria-label="PhotoCamera">
                                            <PhotoCameraIcon fontSize="large"/>
                                        </IconButton>
                                    </span>
                                    <p className="upload-text-style">Drag’n’drop some files here, or click to select files</p> 
                                </div>  
                            )}
                        </Dropzone>
                    </div>
                    <div className= "upload-selected-style">
                        <Tooltip placement="right-start" title="Upload selected files to generate links">
                            <ButtonComponent  onClick={uploadSelected}>Upload selected files</ButtonComponent>
                        </Tooltip>
                    </div> 
                    <div className="display-images">
                        <ul>
                            {files.length > 0 && files.map((file, i) => (
                                <li key={i}>{file.path}</li>
                            ))}
                        </ul>
                    </div>
                </div>
            }
            </div>
        </div>
        );
    ;


}
export default SectionMdHtml;







