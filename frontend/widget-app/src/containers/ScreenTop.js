import React from "react";

const ScreenTop = props =>{
    const {userName} = props;
    return(
        <div className="screen-top">
            <div className="brand">
                <img src="https://www.solutionzone.net/wp-content/uploads/2019/03/Vmware-Training.png" alt="vmware" height="48px" width="60px"/>
                <span className="brand-name brand-text">CARBON BLACK</span>
            </div>
            <span className="brand-text">Hello {userName}</span>
        </div>
    );
}

export default ScreenTop;