
import React from "react";
import IconButton from '@mui/material/IconButton';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

const Actioncells = props => {
    
    const {colDef} = props;
    const {onCellContextMenu} = colDef;
    const {_id} = props.data;
    return (
      <div>
        <IconButton color="primary" aria-label="edit" onClick={() => onCellContextMenu.onClickEditWidget(_id)}>
            <EditIcon />
        </IconButton>
        <IconButton color="primary" aria-label="delete" onClick={() => onCellContextMenu.onClickDeleteWidget(_id)}>
            <DeleteIcon />
        </IconButton>
      </div>
    )
    
  }

  export default Actioncells;