
import React, {useState, useMemo, useCallback}  from "react";
import { AgGridReact} from "ag-grid-react";
import 'ag-grid-enterprise';
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';
import { gridColumnDef } from "./GridDef";

const GridArticles = props =>{

    const {widgets, onClickEditWidget, onClickDeleteWidget} = props;
    const [rowData, setRowData] = useState([]);
    const columnDefs = gridColumnDef(onClickEditWidget, onClickDeleteWidget);

    const onGridReady = useCallback(() => {
        setRowData(widgets);
    }, [widgets]);

    const defaultColDef = useMemo(() => {
        return {
          editable: false,
          enableRowGroup: true,
          enablePivot: true,
          enableValue: true,
          resizable: true,
          flex: 1,
          minWidth: 100,
          filter: true
        };
      }, []);
    const getRowId = useCallback(({data})=> {
        return data._id; 
    }, []);

    return (
    <div className="container-style" >
        <div 
            id="myGrid" 
            className="grid-style ag-theme-alpine"  
            >
            <AgGridReact 
                rowData={rowData}
                columnDefs={columnDefs}
                pagination={true}
                paginationPageSize={10}
                groupHeaders="true"
                rowHeight="40"
                defaultColDef={defaultColDef}
                immutableData={true}
                onGridReady={onGridReady}
                getRowId={getRowId}
            />
        </div>
    </div>)
}

export default GridArticles;
