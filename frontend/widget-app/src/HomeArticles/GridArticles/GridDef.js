import Actioncells from "./ActionCells";
import ReportRenderer from "./ReportRenderer";
export const gridColumnDef = (onClickEditWidget, onClickDeleteWidget)=> {return [
    {
        headerName: "Title",
        field: "title",
        sortable: true,
        filter: true,
        minWidth: 170,
        colId: "title",
    },
    {   
        headerName: "Author",
        sortable: true,
        filter: true,
        field: "author" ,
        minWidth: 100 ,
        colId: "aithor"
    },
    {   
        headerName: "Short Description",
        filter: true,
        field: "shortDesc" ,
        minWidth: 100 ,
        maxWidth: 500,
        colId: "desc"
    },
    { 
        headerName: "Status",
        filter: true,
        sortable: true,
        field: "status",
        minWidth: 100 ,
        colId: "status"
    },
    {   
        headerName: "Published On",
        sortable: true,
        filter: true,
        field: "publishedOn",
        minWidth: 150 ,
        colId: "publishedOn"
        
    },
    { 
        headerName: "Full Report",
        field: "fullReport",
        colId: "report",
        cellRenderer: ReportRenderer
    },
    { 
        headerName: "Actions",
        field: "Actions",
        cellRenderer: Actioncells,
        editable: false,
        colId: "action",
        minWidth: 150,
        onCellContextMenu: {onClickEditWidget, onClickDeleteWidget}
    }
]};