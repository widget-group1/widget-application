import React, {PureComponent} from "react";
import Modal from 'react-responsive-modal';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import WidgetModalContainer from "../containers/WidgetModalContainer";
import {
    openAddWidgetModal,
    openEditWidgetModal,
    closeWidgetModal,
    requestWidgets,
    createWidget,
    updateWidget,
    deleteWidget,
    approveWidget,
    publishWidget,
    rejectWidget,
    requestLogout
  } from '../actions';

import 'react-responsive-modal/styles.css';
import '../style.css';
import Logout from "../Logout/Logout";
import history from "../history";
import GridArticles from "./GridArticles/GridArticles";
import { ButtonComponent } from "../components/ButtonComponent";
import ScreenTop from "../containers/ScreenTop";



const userName = sessionStorage.getItem("name");
class  HomeArticles extends PureComponent{

    constructor(props) {
        super(props);
        this.state = {
          currWidgetId: -1,
          checkConfirmDeletion: false,
          updatedWidgets: props.widgets||[],
          confirmLogoutModal: false
        }
        this.onClickAddWidget = this.onClickAddWidget.bind(this);
        this.onClickModalClose = this.onClickModalClose.bind(this);
        this.submitCreateWidget = this.submitCreateWidget.bind(this);
        this.onClickEditWidget = this.onClickEditWidget.bind(this);
        this.onClickDeleteWidget = this.onClickDeleteWidget.bind(this);
        this.callDeleteWidget = this.callDeleteWidget.bind(this);
        this.callApproveWidget = this.callApproveWidget.bind(this);
        this.callPublishWidget = this.callPublishWidget.bind(this);
        this.callRejectWidget = this.callRejectWidget.bind(this);
        this.clearAll = this.clearAll.bind(this);
        this.onClickLogout = this.onClickLogout.bind(this);
        this.confirmLogout = this.confirmLogout.bind(this);
    }

    componentDidMount(){
      this.props.requestWidgets();
    }

    clearAll(){
      this.setState({updatedWidgets: this.props.widgets});
    }

    onClickAddWidget(){
      this.clearAll();
      this.props.openAddWidgetModal();
    }
    onClickEditWidget(id){
      this.setState({currWidgetId: id});
      this.props.openEditWidgetModal();
    }
    onClickDeleteWidget(id){
      this.setState({currWidgetId: id, checkConfirmDeletion: true});
    }
    onClickModalClose() {
      this.props.closeWidgetModal();
    }
    onClickLogout(){
      this.setState({confirmLogoutModal: true});
    }
    confirmLogout(){
      this.props.requestLogout();
      history.push({
          pathname:"/logout"
      }, ()=>{});
    }
    submitCreateWidget(obj) {
        this.props.createWidget(obj);
        this.props.closeWidgetModal();
    }
    submitUpdateWidget(obj) {
      this.clearAll();
      this.props.updateWidget(obj);
      this.props.closeWidgetModal();
    }
    callDeleteWidget(){
      const {widgets, deleteWidget} = this.props;
      const {currWidgetId}=this.state;
      const widgetToDelete = widgets.find(e=>e._id === currWidgetId);
      deleteWidget(widgetToDelete._id);
      this.setState({checkConfirmDeletion: false, updatedWidgets: widgets});
    }

    callPublishWidget(id){
      this.props.publishWidget(id);
      this.clearAll();
    }
    callApproveWidget(id){
      this.props.approveWidget(id);
      this.clearAll();
    }
    callRejectWidget(id){
      this.props.rejectWidget(id);
      this.clearAll();
    }
    render(){
        const {  addWidgetModal, editWidgetModal, isApprover} = this.props;
        const {currWidgetId, checkConfirmDeletion, updatedWidgets, confirmLogoutModal}=this.state;
        const currWidget  = (currWidgetId!==-1 && editWidgetModal) ? updatedWidgets.find(e=>e._id===currWidgetId) : {};
        
        return (
              <>
                <ScreenTop userName={userName}/>
                <div>
                    <div className="row">
                      <div className="col-9 my-widget-header">
                        <label >Threat Reports {isApprover ? "Admin View": ""}</label>
                      </div>
                      {!isApprover && <div className="col-3 header-sec">
                        <ButtonComponent onClick={this.onClickAddWidget}>Add Article</ButtonComponent>
                        <nav className="nav"> 
                            <ButtonComponent onClick={this.onClickLogout}>Logout</ButtonComponent>
                        </nav>
                      </div>}
                    </div>
                </div>
                <GridArticles 
                    widgets={updatedWidgets}
                    onClickEditWidget={this.onClickEditWidget}
                    onClickDeleteWidget={this.onClickDeleteWidget}
                >

                </GridArticles>
                {/* <WidgetsListContainer 
                    isApprover ={isApprover}
                    list={currList} 
                    onClickEditWidget={this.onClickEditWidget}
                    onClickDeleteWidget={this.onClickDeleteWidget}
                    approveWidget={this.callApproveWidget}
                    publishWidget={this.callPublishWidget}
                    rejectWidget={this.callRejectWidget}
                /> */}
                {checkConfirmDeletion &&
                    <Modal 
                          classNames={{
                            modal: "customModalConfirmDelete",
                          }}
                          open 
                          onClose={()=>this.setState({checkConfirmDeletion: false})}>
                          <div className="delete-confirm">
                              <span>Are you sure you want to delete?</span>
                              <div className="delete-confirm-action-buttons">
                                  <ButtonComponent onClick={this.callDeleteWidget}>confirm</ButtonComponent>
                                  <ButtonComponent onClick={()=>this.setState({checkConfirmDeletion: false})}>cancel</ButtonComponent>
                              </div>
                          </div>
                    </Modal>
                }
                {confirmLogoutModal &&
                    <Logout confirmLogoutUser={this.confirmLogout}/>
                }
                
                {(addWidgetModal || editWidgetModal) &&
                  <Modal 
                      open={addWidgetModal || editWidgetModal} 
                      onClose={this.onClickModalClose} 
                      classNames={{
                        modal: "customModalWidget"
                      }}
                      center >
                    <WidgetModalContainer 
                        isApprover ={isApprover}
                        editType={editWidgetModal}
                        widgetData={currWidget}
                        submitCreateWidget={obj=>this.submitCreateWidget(obj)}
                        submitUpdateWidget={obj=>this.submitUpdateWidget(obj)}
                    />
                  </Modal>}
              </>
          );
    }
        
    
}

HomeArticles.propTypes = {
    addWidgetModal: PropTypes.bool ,
    editWidgetModal: PropTypes.bool,
    requestWidgets: PropTypes.func ,
    createWidget: PropTypes.func ,
    updateWidget: PropTypes.func,
    deleteWidget: PropTypes.func,
    rejectWidget: PropTypes.func,
    approveWidget: PropTypes.func,
    publishWidget: PropTypes.func,
    requestLogout: PropTypes.func
  };
  
const mapStateToProps = (state) => {
  return {
    addWidgetModal: state.addWidgetModal ,
    editWidgetModal: state.editWidgetModal,
    loading: state.loading,
    widgets: state.widgets     
  };
}
const mapDispatchToProps = (dispatch) => {
  return (
    bindActionCreators(
      {
        openAddWidgetModal ,
        openEditWidgetModal,
        closeWidgetModal,
        requestWidgets ,
        createWidget,
        updateWidget,
        deleteWidget,
        approveWidget,
        publishWidget,
        rejectWidget,
        requestLogout
      }, dispatch
    )
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeArticles);
